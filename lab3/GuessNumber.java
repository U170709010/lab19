
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		int tries = 0;
		
		boolean win = false;
		
		while (win = false) {
		
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it?: ");
		
		int guess = reader.nextInt(); //Read the user input
		tries++;
		
		if ( guess == number) {
			win = true;
		}
		else if ( guess < number) {
			System.out.println("Your guess is too low.");
		}
		if (guess > number ) {
			System.out.println("Your guess is too high. ");
		}
		}	
		System.out.println("You win!");
		System.out.println("The number was" + number);
		System.out.println("You won after " + tries + "attempts");
		
		
	}
	
}	


	

